\chapter{Variables, Expresiones y Sentencias}

Una de las más poderosas características de un lenguaje de programación
es la habilidad para manipular {\bf variables}. En términos generales, 
una variable es el nombre que hace referencia a un valor. Sería más preciso
decir que una variable es un contenedor que tiene un nombre y almacena un
valor.
\index{variable}


\section{Sentencias de Asignación}
\seclabel{variables}
\index{asignación!de variable}
\index{sentencia!asignación}
\index{operador!\texttt{=} (asignación)}


Una {\bf sentencia de asignación} usa el signo de igualdad {\tt =}
y asigna un valor a una variable, pero, antes de que asignes un valor 
a una variable, debes primero crear la variable a través de una
declaración (si todavía no existe):
\begin{verbatim}
> my $mensaje;          # declaración de una variable, aún sin valor
> $mensaje = 'And now for something completely different';
And now for something completely different
> my $número = 42;      # declaración de una variable y asignación
42
> $número = 17;         # nueva asignación
17
> my $phi = 1.618033988;
1.618033988
>
\end{verbatim}
%
\index{asignación}
\index{sentencia!asignación}
\index{declaración!variable}
\index{variable!declaración}

Este ejemplo lleva a cabo cuatro asignaciones. La primera asigna una cadena de
texto a una nueva variable llamada {\tt \$mensaje}, la segunda asigna el número
entero {\tt 42} a {\tt \$número}, la tercera reasigna el entero {\tt 17} a
{\tt \$número}, y la cuarta asigna el valor aproximado del número áureo a
{\tt \$phi}.

Aquí hay dos características sintácticas importantes que debes entender.

Primero, en Perl, los nombres de las variables comienzan con un 
símbolo conocido como un \emph{sigilo}, es decir, un carácter no alfanumérico
como \verb|$|, \verb|@|, \verb|%|, \verb|&|, y otros más. Este carácter
especial nos informa a nosotros y al compilador de Perl (el programa que lee
el código de nuestro programa y lo transforma en instrucciones
de computadoras) el tipo de variable que es. Por ejemplo, el 
carácter \verb|$| indica que las variables más arriba son todas \emph{variables escalares},
lo cual significa que ellas pueden almacenar un solo valor
en cualquier momento. Veremos más adelante otros tipos de variables las
cuales pueden contener más de un solo valor.
\index{sigilo}
\index{escalar}
\index{variable!escalar}

Segundo, nota como las tres variables más arriba son 
introducidas con la palabra clave {\tt my}, que es una manera
de declarar una variable nueva. Cuando tu creas una variable nueva
en Perl, necesitas \emph{declararla}, o sea, le dejas saber a Perl
que vas a usar una variable nueva; esto se hace comúnmente con la
palabra clave {\tt my}, la cual declara una variable \emph{léxica}.
Explicaremos luego lo que es una variable léxica pero por ahora,
digamos que te permite crear una variable local en una parte 
limitada de tu código. Una de las consecuencias buenas del requerimiento
de declarar variables antes de que las uses es que, si tu cometes
un error al escribir el nombre de una variable, el compilador 
usualmente será capaz de decirte que estás usando una variable que
no ha sido declarada y como resultado te ayudará a encontrar tu error. 
Esto tiene grandes complicaciones las cuales examinaremos más tarde.
\index{declarador!my}
\index{declarando variables}
\index{variable!declaración}
\index{lexical}
\index{variable!léxica}

Cuando escribimos al inicio de esta sección que una variable debe
ser declarada antes de ser usada (o solo cuando se usa), esto
significa que la declaración tiene que ser antes (o al punto de) 
del primer uso de la variable en el archivo de texto que contiene el 
programa. Veremos más adelante que los programas no son ejecutados
necesariamente de arriba hacia abajo en el orden en el que las líneas
o código aparecen en el programa; aún así, la declaración de la variable
debe ocurrir antes de su uso en el archivo de texto que contiene el programa.

Si olvidas declarar una variable, obtienes un error sintáctico:
\index{error!sintáctico}

\begin{verbatim}
> $número = 5;
===SORRY!=== Error while compiling <unknown file>
Variable '$número' is not declared
at <unknown file>:1
------> <BOL><HERE>$número = 5;
>
\end{verbatim}
%
Ten presente que podrías obtener mensajes diferentes de
error dependiendo de la versión de Rakudo que ejecutes.
El mensaje más arriba se obtuvo en Febrero 2016;
con una versión más nueva (Octubre 2016), el mismo error
se muestra en una forma algo más organizada:
\begin{verbatim}
>
> $número = 5;
===SORRY!=== Error while compiling:
Variable '$número' is not declared
at line 2
------> <BOL><HERE>$número = 5;
>
\end{verbatim}

\index{diagrama!estado}

Una manera común de representar variables en papel es escribir el 
nombre con una flecha apuntando a su valor. Este tipo de dibujo se le conoce
como un {\bf diagrama de estado} porque muestra en qué estado cada una de las
variables se encuentra (imagínalo como el estado mental de la variable).
La \figref{state2} muestra el resultado del ejemplo anterior.

\begin{figure}
\centerline
{\includegraphics[scale=0.6]{figs/test_5.pdf}}
\caption{Diagrama de estado.}
\figlabel{state2}
\end{figure}



\section{Nombres de las Variables}
\index{variable}

Los programadores generalmente eligen nombres significativos
para sus variables---los nombres documentan el uso de la variable.

Los nombres de las variables pueden ser tan largos como tu desees.
Ellos pueden contener letras y números, pero las variables
definidas por el usuario no pueden comenzar con un número. Los nombres de las
variables son sensibles al uso de letras mayúsculas y minúsculas, 
es decir, {\tt \$mensaje} no es la misma variable que {\tt \$Mensaje}
o {\tt \$MENSAJE}. Es legal usar letras mayúsculas, pero es convencional
usar solo letras minúsculas para la mayoría de los nombres de las
variables. No obstante, algunas personas prefieren usar
{\tt \$TipoTítulo}~\engterm{camel case} para los nombres de sus variables
o hasta {\tt \$MAYÚSCULAS} para algunas variables especiales.
\index{minúscula}
\index{mayúscula}
\index{título}
\index{caso!minúscula}
\index{caso!mayúscula}
\index{caso!título}


\index{Unicode}
\index{ASCII}
A diferencia de muchos otros lenguajes de programación, Perl~6 
no requiere que las letras y dígitos en los nombres de las variables
sean únicamente ASCII. Puedes usar cualquier tipo de letras Unicode, es decir,
letras de cualquier otro lenguaje en el mundo, así que, por ejemplo,
{\tt \$brücke}, {\tt \$payé} o {\tt \$niño} son nombres de variables
válidos, los cuales pueden ser usados por programadores que hablan 
otros idiomas diferentes al inglés (siempre y cuando estos caracteres
Unicode sean correctamente manipulados por tu editor de texto y tu
configuración de pantalla). De la misma manera, en vez de usar 
\verb"$phi" para el nombre de la variable del número áureo,
podríamos haber usado la \emph{letra griega minúscula phi},
\verb'φ' (Unicode code point U+03C6). Igualmente, podríamos
haber usado la \emph{letra griega minúscula pi}, $\pi$, 
para la muy conocida razón de la circunferencia del círculo
al diámetro:
\index{número!aúreo}
\index{Unicode}
\index{phi}
\index{pi}

\begin{verbatim}
> my $φ = (5 ** .5 + 1)/2;       # número aúreo
1.61803398874989
> say 'Variable $φ = ', $φ;
Variable $φ = 1.61803398874989
> my $π = 4 * atan 1; 
3.14159265358979
> # podrías también las constante integrada pi o π:
> say pi
3.14159265358979
\end{verbatim}

El carácter de barra baja, \verb"_", puede aparecer en cualquier
parte del nombre de una variable. Usualmente se usa en nombres con
múltiples palabras, tal como \verb"$tu_nombre" o
\verb"$airspeed_of_unladen_swallow". 
\index{barra baja}

Hasta puedes usar guiones para crear lo que se conoce como
``kebab case''\footnote{Llamado así porque las variables aparecen estar
atravesadas como las piezas de comida preparadas para una barbacoa.}
y nombrar esas variables \verb"$tu-nombre" o \verb"$airspeed-of-unladen-swallow",
y esto las puede hacer más legibles: un guion \verb'-' es válido en una variable
siempre y cuando sea inmediatamente seguido por un carácter alfanumérico.
Por ejemplo, \verb"$doble-clic" or \verb"$la-niña" son nombres legítimos
de variables. Análogamente, puedes usar un apóstrofo \verb"'" 
(también conocido como comilla simple) entre las letras, así que 
\verb"$isn't" o \verb"$o'brien's-age" son identificadores válidos. 
\index{raya}
\index{apóstrofo}
\index{comillas!simples}
\index{caso!kebab}


Si les da un nombre ilegal a una variable, obtienes un error sintáctico:
\index{error!sintáctico}

\begin{verbatim}
> my $76trombones = 'big parade'
===SORRY!=== Error while compiling <unknown file>
Cannot declare a numeric variable
at <unknown file>:1
------> my $76<HERE>trombones = "big parade";
>
> my $more§ = 100000;
===SORRY!=== Error while compiling <unknown file>
Bogus postfix
at <unknown file>:1
------> my $more<HERE>§ = 100000;
(...)
\end{verbatim}
%
{\tt \$76trombones} es ilegal porque comienza con un número.
{\tt \$more§} es ilegal porque contiene un carácter ilegal, {\tt
§}. 

Si alguna vez has usado otro lenguaje de programación y 
te has tropezado con un mensaje terso tal como {\tt"SyntaxError: invalid syntax"},
notarás que los diseñadores de Perl~6 han hecho un gran esfuerzo
para proveerte con mensajes de error que sean detallados, útiles
y significativos.
\index{mensaje de error}

Muchos lenguajes de programación tienen \emph{palabras claves} o
\emph{palabras reservadas} que son parte de la sintaxis, tales como
{\tt if}, {\tt while}, o {\tt for}, y por lo tanto no pueden ser usadas
para identificar variables porque eso crearía ambigüedad. Dicho problema
no existe en Perl: dado que los nombres de las variables comienzan con un sigilo,
el compilador es siempre capaz de diferenciar entre una palabra clave
y una variable. Nombres tales como {\tt \$if} o {\tt \$while} son
sintácticamente identificadores válidos de variables en Perl 
(si estos nombres hacen sentido es un asunto diferente).
\index{sigilo}
\index{palabra!clave}
\index{palabra!reservada}


\section{Expresiones y Sentencias}
\seclabel{expr_and_statements}

Una {\bf expresión} es una combinación de términos y operadores.
Los términos pueden ser variables o literales, es decir, valores constantes tales
como un número o una cadena de texto. Al igual que un valor, una variable
es también considerada una expresión. Así que todo las siguientes
son expresiones legales:
\index{expresión}
\index{término}
\index{literal}

\begin{verbatim}
> 42
42
> my $n = 17;
17
> $n;
17
> $n + 25;
42
>
\end{verbatim}
%
Cuando escribes una expresión en el prompt, el interpretador
la {\bf evalúa}, lo que significa que encuentra el valor de la expresión.
En este ejemplo, {\tt \$n} tiene el valor 17 y {\tt \$n + 25} tiene
el valor 42.
\index{evaluar}

Una {\bf sentencia} es una unidad de código que tiene un efecto, 
tal como crear una variable o mostrar un valor, y usualmente
necesita terminar con un punto y coma {\tt ;} (aunque el punto y coma
puede algunas veces ser omitido como veremos más adelante):
\index{sentencia}
\index{punto y coma}

\begin{verbatim}
> my $n = 17;
17
> say $n;
17
\end{verbatim}
%

La primera línea es una sentencia de asignación que asigna un 
valor a {\tt \$n}. La segunda línea es una sentencia de impresión
que muestra el valor de {\tt \$n}.

Cuando escribes una sentencia y después presiona {\tt Enter},
el interpretador la {\bf ejecuta}, lo que significa que hace 
lo que la sentencia dicta.
\index{ejecutar}

Una sentencia puede ser combinada con expresiones usando los operadores
aritméticos. Por ejemplo, podrías escribir:
\index{operador}

\begin{verbatim}
> my $respuesta = 17 + 25;
42
> say $respuesta;
42
\end{verbatim}
%

El símbolo \verb|+| es obviamente el operador de adición y,
después de la sentencia de asignación, la variable \verb|$respuesta|
contiene el resultado de la adición. Los términos en cada lado del operador
(aquí 17 y 25) son usualmente llamados los \emph{operandos}
de la operación (una adición en este caso).
\index{operando}
\index{operador!\texttt{+} (adición)}
\index{asignación}

Nota que el REPL actualmente muestra el resultado de la asignación
(la primera línea con ``42''), así que la sentencia de impresión
no era realmente necesaria en este ejemplo \emph{en el REPL};
desde aquí en adelante, para ser breve, generalmente omitiremos las sentencias
de impresión en los ejemplos donde el REPL muestra los resultados.
\index{REPL}

En algunos casos, puedes querer añadir algo a una variable y 
asignar el resultado a la misma variable. Esto se puede escribir
así:

\begin{verbatim}
> my $respuesta = 17;
17
> $respuesta = $respuesta + 25;
42
\end{verbatim}
%

Aquí, \verb"$respuesta" es primero declarada con un valor de 17. La siguiente
sentencia asigna a  \verb"$respuesta" el valor actual de  \verb"$respuesta"
(por ejemplo, 17) + 25. Esta operación es tan común en Perl, como en otro lenguajes de
programación, que tiene un atajo:

\begin{verbatim}
> my $respuesta = 17;
17
> $respuesta += 25;
42
\end{verbatim}
%

\index{operador!\texttt{+=} (aumento y asignación)}
El operador \verb"+=" combina el operador de la adición aritmética 
y el operador de asignación para modificar un valor y aplicar 
el resultado a una variable en un solo paso, así que 
\verb"$n += 2" quiere decir: toma el valor actual de \verb|$n|, agrega
2, y asigna el resultado a \verb|$n|. Esta sintaxis funciona con todos
los operadores aritméticos. Por ejemplo, \verb|-=| realiza una
sustracción y una asignación, \verb|*=| una multiplicación y una asignación, etc.
Además de los operadores aritméticos, también puede ser usado con otros operadores 
tal como el operador de la concatenación de cadena de texto que veremos
más adelante.

Agregar 1 a una variable es una versión muy común de esto, 
y como resultado, existe un atajo, el operador de \emph{incremento},
el cual incrementa su argumento por uno, y devuelve el valor incrementado: 
\index{operador!\texttt{++} (aumento)}

\begin{verbatim}
> my $n = 17;
17
> ++$n;
18
> say $n;
18
\end{verbatim}
%
Esto se conoce como el operador de incremento prefijo, porque el operador \verb|++|
se coloca antes que la variable a ser incrementada. También hay una versión sufija (posfija),
\verb|$n++|, la cual devuelve el valor actual y después incrementa la variable por uno. 
No haría ninguna diferencia en el fragmento de código más arriba, pero el resultado puede
ser diferente en expresiones un poco más complejas.

También hay un operador de decremento \verb|--|, el cual disminuye 
su argumento por uno y existe en la forma prefija y sufija. 
\index{operador!\texttt{--} (aumento)}

\section{Modo Script}

Hasta ahora hemos ejecutado Perl en el {\bf modo interactivo}, lo que 
significa que tu interactúas directamente con el interpretador (REPL).
El modo interactivo es una buena manera de comenzar, aunque si estás trabajando 
con más de varias líneas de código, puede ser un poco torpe y hasta tedioso.
\index{modo!interactivo}

La alternativa es usar un editor de texto y guardar el código en un archivo de 
texto conocido como un {\bf script} y después ejecutar el interpretador en el 
{\bf modo script} para ejecutar el script. Por convención, los scripts de Perl~6 
tienen nombres que terminan con {\tt .pl}, {\tt .p6} o {\tt .pl6}.
\index{script}
\index{modo!script}

Asegúrate de usar un \emph{editor de texto} y no un \emph{programa de
procesamiento de texto} (como MS Word, OpenOffice o LibreOffice Writer). Hay un
gran número de editores de texto gratis disponibles. En Linux, podrías usar
\emph{vi} (o \emph{vim}), \emph{emacs}, \emph{gEdit}, o \emph{nano}. En Windows,
puedes usar \emph{notepad} (muy limitado), o \emph{notepad++}. También hay 
editores multiplataforma o entorno de desarrollo integrado (IDEs por su siglas
en inglés) los cuales proveen la funcionalidad de un editor de texto. Entre
ellos están \emph{padre}, \emph{eclipse}, o \emph{atom}. Muchos de estos proveen
varias funcionalidades de resaltado de sintaxis, que pueden ayudarte a usar
la sintaxis correcta (y encontrar algunos errores sintácticos).
\index{resaltado de sintaxis}
\index{editor de texto!emacs}
\index{editor de texto!vi}
\index{editor de texto!vim}
\index{editor de texto!gEdit}
\index{editor de texto!padre}
\index{editor de texto!eclipse}
\index{editor de texto!nano}
\index{editor de texto!notepad++}
\index{editor de texto!atom}

Una vez que has guardado tu código en un archivo de texto (por ejemplo,
\verb|mi_script.pl6|), puedes ejecutar el programa mediante el siguiente
comando en el prompt del sistema operativo (por ejemplo en una consola de 
Linux or en una ventana \verb|cmd| en Windows):
\begin{verbatim}
perl6 mi_script.pl6
\end{verbatim}

Dado que Perl provee ambos modos, 
puedes ejecutar piezas de código en el modo interactivo 
antes que las pongas en un script. Pero existen diferencias entre 
el modo interactivo y el modo script que pueden confundir.
\index{modo!interactivo}
\index{modo!script}

Por ejemplo, si estás usando el interpretador de Perl~6 como una
calculadora, podrías escribir:

\begin{verbatim}
> my $millas = 26.2;
26.2
> $millas * 1.61;
42.182
\end{verbatim}

La primera línea asigna un valor a {\tt \$millas} y muestra ese valor.
La segunda línea es una expresión, así que el interpretador la evalúa
y muestra el resultado. Resulta que un maratón es alrededor de 42~kilómetros.

Pero si escribes el mismo código en un script y lo ejecutas, no obtienes
ningún resultado. En el modo script, una expresión por sí misma no tiene
ningún efecto visible. Perl actualmente evalúa la expresión, pero no muestra
el valor a menos que se lo indiques:

\begin{perl6code}
my $millas = 26.2;
say $millas * 1.61;
\end{perl6code}

Este comportamiento puede ser confuso al principio. Examinemos
el por qué.

Un script usualmente contiene una secuencia de sentencias. Si hay más 
de una sentencia, los resultados aparecen uno por uno al 
tiempo que las sentencias de impresión se ejecutan.

Por ejemplo, considera el siguiente script:

\begin{perl6code}
say 1;
my $x = 2;
say $x;
\end{perl6code}
%
Produce el siguiente resultado:

\begin{verbatim}
1
2
\end{verbatim}
%
La sentencia de asignación no produce ningún resultado.

Para comprobar tu entendimiento, escribe las siguientes sentencias
en el interpretador de Perl y observa lo que hacen:

\begin{perl6code}
5;
my $x = 5;
$x + 1;
\end{perl6code}

Ahora pon la mismas sentencias en un script y ejecútalo. 
¿Cuál es el resultado? Transforma cada expresión en una sentencia 
de impresión y después ejecuta el script modificado otra vez.

\section{Modo de una sola línea}

Perl también tiene un \emph{modo de una sola línea} (modo one-liner), el cual
te permite escribir un script bien corto directamente en el prompt del sistema
operativo. Debido a esto, dichos scripts son conocidos como \emph{one-liners}.
En Windows, podría lucir de la siguiente manera:
\index{modo!one-liner}
\seclabel{one-liner-mode}

\begin{verbatim}
C:\Users\Laurent>perl6 -e "my $valor = 42; say 'La respuesta es ', $valor;"
La respuesta es 42

\end{verbatim}

La opción {\tt -e} le dice al compilador que el script
a ser ejecutado no está guardado en un archivo de texto sino
que en cambio está escrito en el prompt entre comillas dobles
inmediatamente después de dicha opción.

En Unix y Linux, reemplazarías las comillas dobles con apóstrofos
(o comillas simples) y los apóstrofos con comillas dobles:
\index{apóstrofo}
\index{comillas}

\begin{verbatim}
$  perl6 -e 'my $valor = 42; say "La respuesta es $valor";'
La respuesta es 42

\end{verbatim}

El one-liner más arriba puede no parecer útil, pero los
one-liners desechables pueden ser muy prácticos para realizar
operaciones simples, tal como modificar un archivo que no
está formateado apropiadamente, sin tener que guardar un script
en un archivo separado antes de ejecutarlo.  

No daremos ningunos detalles adicionales sobre el modo 
de una sola línea, pero daremos más ejemplos útiles más
adelante en este libro; por ejemplo,
\subsecref{one-liner-example},
\subsecref{rot13_oneliner} (resolviendo el ejercicio ``rot-13''), o
\subsecref{sol_cartalk} (resolviendo el ejercicio sobre
letras dobles consecutivas). 



\section{Orden de Operaciones}
\index{PEMDAS}
\index{precedencia!operador}

Cuando una expresión contiene más de un operador, el orden 
de evaluación depende en el {\bf orden de las operaciones} o 
la \emph{precedencia de los operadores}. Para los operadores matemáticos, Perl
sigue la convención matemática. El acrónimo {\bf PEMDAS}\footnote{Los estudiantes 
estadounidenses se les enseña a usar el mnemónico 
"Please Excuse My Dear Aunt Sally" para recordar el orden 
correcto de las letras en el acrónimo.

En español, podrías utilizar "Por Favor, Excusa Mi Dragón Azul, Sancho" para
recordar el orden correcto.} es una manera muy útil de recordar
las reglas:
 
\begin{itemize}

\item {\bf P}aréntesis tienen la mayor precedencia y pueden ser 
usados para forzar una expresión a evaluar en el orden que deseas.
Dado que las expresiones en paréntesis son evaluadas primero, 
{\tt 2 * (3-1)} es 4, y {\tt (1+1)**(5-2)} es 8.  También puedes usar 
paréntesis para hacer una expresión más legible, como en
{\tt (\$minuto * 100) / 60}, aunque no cambia el resultado.
\index{operador!\texttt{()}}

\item {\bf E}xponente (potenciación) es la siguiente en el nivel de precedencia,
así que {\tt 1 + 2**3} es 9 (1 + 8), no 27, y {\tt 2 * 3**2} es 18, no 36.
\index{operador!\texttt{**} (potenciación)}

\item {\bf M}ultiplicación y {\bf D}ivisión tiene mayor precedencia
  que {\bf A}dición y {\bf S}ustracción.  Por lo tanto, {\tt 2*3-1} es 5, no
  4, y {\tt 6+4/2} es 8, no 5.
\index{operador!\texttt{*} (multiplicación)}
\index{operador!\texttt{/} (división)}

\item Operadores con la misma precedencia son usualmente evaluados
de izquierda a derecha (excepto la potenciación). Así que en la expresión
{\tt \$grados / 2 * pi}, la división ocurre primero y el resultado 
es multiplicado por {\tt pi}, el cual no es el resultado esperado. (Nota que 
{\tt pi} no es una variable, sino una constante predefinida en Perl~6, y por lo
tanto no requiere un sigilo.) Para dividir por $2 \pi$, puedes usar paréntesis: 
\index{sigilo}
\index{pi}
  
\begin{perl6code}
my $resultado = $grados / (2 * pi);  
\end{perl6code}  
 
o escribir 
  {\tt \$grados / 2 / pi} o {\tt \$grados / 2 / $\pi$}, lo cual
  divide \verb'$grados' por 2, y después divide el resultado de esa
  operación por $\pi$ (el cual es equivalente a \verb'$grados'
  dividido por $2 \pi$).

\end{itemize}

Trato de no recordar la precedencia de los operadores. Si no puedo 
determinar la precedencia al mirar la expresión, uso paréntesis para
hacerlo obvio. Si no sé cuál de dos operadores tiene la mayor precedencia, 
entonces la siguiente persona manteniendo mi código podría tampoco saberlo.
\index{precedencia}
\index{paréntesis}


\section{Operaciones de Cadena de Texto}
\seclabel{string_operations}
\paglabel{string_operations}
\index{cadena de texto!operación}
\index{operador!cadena de texto}
\index{coerción}
\index{tipo!coerción}

En general, no puedes realizar operaciones matemáticas con las cadenas de texto,
a menos que las cadenas de texto se parezcan tanto a los números que 
Perl las transforma o \emph{coacciona} en números y todavía hace sentido.
Así que los siguientes casos son ilegales:

\begin{verbatim}
'2'-'1a'    'eggs'/'easy'    'third'*'a charm'
\end{verbatim}
%

Por ejemplo, esto produce un error:

\begin{verbatim}
> '2'-'1a'
Cannot convert string to number: trailing characters after number 
in '1?a' (indicated by ?)
  in block <unit> at <unknown file>:1
\end{verbatim}
%

Pero las siguientes expresiones son válidas porque estas cadenas de texto
pueden ser coaccionadas en números sin ninguna ambigüedad:
\begin{verbatim}
> '2'-'1'
1
> '3'/'4'
0.75
\end{verbatim}
%

El operador \verb'~' realiza la {\bf concatenación de cadenas de texto}, 
lo cual quiere decir que une las cadenas de texto al enlazarlas de
extremo a extremo. Por ejemplo:
\index{concatenación!de cadenas de texto}
\index{cadena de texto!concatenación}

\begin{verbatim}
> my $primera = 'hidro'
hidro
> my $segunda = 'avión'
avión
> $primera ~ $segunda
hidroavión
\end{verbatim}
%
El operador {\tt x} también funciona con cadenas de texto; básicamente
realiza repeticiones. Por ejemplo:
\index{repetición!de cadenas de texto}
\index{operador!\texttt{x} (repetición de cadenas de texto)}

\begin{verbatim}
> 'ab' x 3;
ababab
> 42 x 3
424242
> 3 x 42
333333333333333333333333333333333333333333
\end{verbatim}

Nota que, aunque el operador {\tt x} se parece al operador
de multiplicación que escribimos a mano, {\tt x} obviamente 
no es conmutativo, contrario al operador de multiplicación 
{\tt *}. El primer operando es una cadena de texto o es \emph{coaccionado}
en una cadena de texto (es decir, transformado en una cadena de texto:
{\tt 42} es coaccionado en {\tt '42'}), y el segundo operando
tiene que ser un número o algo que pueda ser transformado en 
un número.
\index{comuntatividad}
\index{coerción}


\section{Comentarios}
\index{comentario}

A medida que los programas crecen y se vuelven más complicados,
se vuelven menos legibles. Los lenguajes formales son densos, y es usualmente
difícil mirar a un fragmento de código y descifrar lo que está haciendo,
o por qué lo está haciendo.

Por esta razón, es muy buena idea agregar notas a tus programas que expliquen
en un lenguaje natural lo que el programa hace. Estas notas son conocidas como
{\bf comentarios}, y comienzan con el símbolo \verb|#|:

\begin{perl6code}
# computar el porcentaje de la hora que ha pasado
my $porcentaje = ($minuto * 100) / 60;
\end{perl6code}
%
En este caso, el comentario aparece en una línea por sí mismo. También puedes
colocar comentarios al final de una línea:

\begin{perl6code}
$porcentaje = ($minuto * 100) / 60;     # porcentaje de una hora
\end{perl6code}
%
Todo desde el inicio de {\tt \#} al final de la línea es ignorado---lo que
significa que no tiene un efecto en la ejecución del programa.

Los comentarios son más útiles cuando documentan características del código
que no son obvias. Es razonable asumir que el lector puede descifrar \emph{lo} que
hace el código; es más útil explicar el {\em por qué}.

Este comentario es redundante e inservible debido a que no brinda ninguna
información que no pueda deducirse del mismo código:

\begin{perl6code}
my $velocidad = 5;        # asignar 5 a $velocidad
\end{perl6code}
%
Por el contrario, este comentario contiene información útil que no está
presente en el código:

\begin{perl6code}
my $velocidad = 5;     # la velocidad es en metros/segundos. 
\end{perl6code}
%
Los nombres de variables que son buenos y adecuados pueden reducir la necesidad
de comentarios, pero nombres muy largos pueden hacer que las expresiones sean
difíciles de leer, por lo tanto debe existir un balance.
\index{nombre de variable}


\section{Depuración de Programas}
\index{depuración}
\index{error}

Tres tipos de errores pueden ocurrir en un programa: errores
sintácticos, errores al tiempo de ejecución, y  errores semánticos.
Es muy útil saber distinguirlos para rastrearlos más rápidamente.

\begin{description}

\item[Error sintáctico] La ``sintaxis'' se refiere a la estructura de un 
programa. Por ejemplo, los paréntesis deben estar en parejas, así que 
{\tt (1 + 2)} es legal mientras {\tt 8)} es un \emph{error sintáctico}.
\footnote{Aquí estamos usando ``error sintáctico'' como un cuasi-sinónimo
para ``error al tiempo de compilación''; ellos no son exactamente la misma cosa
(en teoría, puedes tener errores sintácticos que no son errores al tiempo
de compilación y viceversa), pero se pueden considerar aquí lo mismo por razones
prácticas. En Perl~6, errores al tiempo de compilación tienen la cuerda de texto
``===SORRY!==='' al inicio del mensaje de error.}

\index{error!sintaxis}
\index{mensaje de error}
\index{sintaxis} 
\index{sintaxis!error}

Si en tu programa hay un error sintáctico, Perl muestra 
un mensaje de error y abandona la ejecución sin siquiera 
comenzar a ejecutar el programa. Durante las primeras semanas de 
tu carrera de programación, podrías invertir mucho tiempo rastreando
errores sintácticos. A medida que ganas experiencia, cometerás menos errores
y los encontrarás más rápido.


\item[Error al tiempo de ejecución] El segundo tipo de error es un 
error al tiempo de ejecución, llamado así porque el error no aparece hasta que 
el programa ha comenzado a ejecutarse. Estos errores también son conocidos
como \emph{excepciones} porque ellos usualmente indican que algo excepcional
(y malo) ha ocurrido. 
\index{error!al tiempo de ejecución}
\index{excepción} 
\index{lenguaje!seguro}

Los errores al tiempo de ejecución son raros en los programas simples 
que verás en los primeros capítulos, así que podría pasar un rato antes
que encuentres uno. Ya hemos visto un ejemplo de tales errores, aunque, 
al inicio de la \secref{string_operations} (\pagref{string_operations}),
cuando intentamos sustraer \verb"'2'-'1a'".


\item[Error semántico] El tercer tipo de error es \emph{semántico}, lo que
significa que está relacionado con el significado. Si en tu programa hay un error
semántico, el programa se ejecutará sin generar mensajes de error. Específicamente,
hará lo que le \emph{dijiste} que hiciera, pero no lo que habías \emph{previsto}.
\index{error!semántico}
\index{mensaje de error}

La identificación de errores semánticos puede ser complicada porque requiere
que trabajes de atrás hacia adelante mediante la observación de la salida
del programa y tratando de descifrar lo que el programa está haciendo.

\end{description}


\section{Glosario}

\begin{description}

\item[Variable]  Informalmente, un nombre que hace referencia a un valor.
En términos precisos, una variable es un contenedor que tiene un nombre y
almacena un valor.
\index{variable}

\item[Asignación]  Una sentencia que asigna un valor a una variable.
\index{asignación}

\item[Diagrama de estado]  Una representación gráfica de un conjunto de variables
y los valores a los que hacen referencia.
\index{diagrama!de estado}

\item[Palabra clave]  Una palabra reservada que es usada para parsear
un programa; en muchos lenguajes, no puedes usar palabras claves tales como
{\tt if}, {\tt  for}, y {\tt while} como nombres de variables.
Este problema usualmente no ocurre en Perl porque los nombres de variables
comienzan con un \emph{sigilo}.
\index{palabra!clave}
\index{sigilo}

\item[Operando] Un valor o término al lado de un operador que es usado
en su evaluación.
\index{operando}

\item[Término]  Una variable o valor literal.
\index{término}

\item[Expresión]  Una combinación de operadores y términos que 
representan un valor único.
\index{expresión}

\item[Evaluar]  Simplificar una expresión al realizar las operaciones 
para obtener un valor único.
\index{evaluar}

\item[Sentencia]  Una sección de código que representa un comando o una acción.
Hasta ahora, las sentencias que hemos visto son las asignaciones y las
sentencias de impresión. Las sentencias usualmente terminan con un punto y coma.
\index{sentencia}

\item[Ejecutar]  Poner una sentencia en acción y hacer lo que dice.
\index{ejecutar}

\item[Modo interactivo (o modo interpretador)] Una manera de usar el interpretador 
de Perl al escribir código en el prompt.
\index{modo!interactivo}

\item[Modo script] Una manera de usar el interpretador 
de Perl para leer código desde un script y ejecutarlo.
\index{modo!script}

\item[Modo de una sola línea (one-liner)] Una manera de usar el interpretador 
de Perl para leer código pasado al prompt del sistema operativo 
y ejecutarlo.
\index{modo!one-liner}

\item[Script] Un programa almacenado en un archivo de texto.
\index{script}

\item[Order de operaciones]  Reglas que dictan el orden en el que las expresiones
que contienen varios operadores y operandos son evaluadas.
También se conoce como precedencia de los operadores.
\index{orden!de las operaciones}
\index{precedencia!operador}

\item[Concatenar]  Unir dos cadenas de texto de extremo a extremo.
\index{concatenación}

\item[Comentario]  Información en una programa que está dirigida a otros programadores
(o cualquier persona leyendo el código fuente) y que no tiene ningún efecto en 
la ejecución del programa.
\index{comentario}

\item[Error sintáctico]  Un error en un programa que hace imposible
analizarlo sintácticamente (y por lo tanto imposible de compilarlo y ejecutarlo).
\index{sintaxis!error}
\index{error!sintáctico}

\item[Excepción]  Un error que es detectado mientras que el programa se ejecuta.
\index{excepción}
\index{error!excepción}

\item[Semántica]  El significado de un programa.
\index{semántica}

\item[Error semántico] Un error en un programa que causa que haga algo 
diferente a lo que programador pretendía hacer.
\index{error!semántico}

\end{description}


\section{Ejercicios}

\begin{exercise}

Repetimos nuestro consejo mencionado en el capítulo anterior,
cuando aprendas algo nuevo, deberías intentarlo en el modo interactivo
(de REPL) y cometer errores a propósito para ver que es lo que no funciona.
\index{modo!interactivo}
\index{REPL}

\begin{itemize}

\item Hemos visto que {\tt \$n = 42} es legal. ¿Y {\tt 42 = \$n}?

\item ¿Qué acerca de {\tt \$x = \$y = 1}? (Pista: nota que 
tendrás que declarar ambas variables, por ejemplo con una sentencia
tal como {\tt my \$x; my \$y;} o posiblemente {\tt my (\$x, \$y);},
antes de que ejecutes lo anterior.)

\item En algunos lenguajes, las sentencias no tienen que terminar con un
punto y coma, {\tt ;}. ¿Qué pasa en el modo script si omites un punto y
coma al final de una sentencia de Perl?
\index{punto y coma}

\item ¿Qué pasa si pones un punto al final de una sentencia?

\item En notación matemática puedes multiplicar $m$ y $n$ así: $m n$.
¿Qué pasa si tratas de hacerlo en Perl?

\end{itemize}

\end{exercise}


\begin{exercise}

Practica usando el interpretador de Perl como una calculadora:
\index{calculadora}

\begin{enumerate}

\item El volumen de una esfera con radio $r$ es $\frac{4}{3} \pi r^3$.
¿Cuál es el volumen de una esfera con radio 5?

\item Supón que el precio inicial de un libro es \$24.95,
pero la librería obtiene un 40\% de descuento. El envío
cuesta \$3 por la primera copia y 75 centavos por cada copia adicional.
¿Cuál es el precio total de 60 copias?

\item Si dejo la casa a la 6:52 a.m. y corro 1 milla a paso suave
(8 min. 15 seg. por milla), después 3 millas a paso rápido 
(7 min. 12 seg. por milla) y 1 milla a paso suave otra vez.
¿Qué hora es cuando termino con mi ejercicio?
\index{paso suave}

\end{enumerate}
\end{exercise}

