\chapter{Algunos Consejos Finales}

\begin{quote}
\raggedleft
\emph{Todo el mundo sabe que la depuración es dos veces más difícil\\
que escribir un programa en primer lugar. Así que si eres tan astuto\\
como puedas cuando lo escribes, cómo podrás depurarlo? }\\
--- Brian Kernighan, "The Elements of Programming Style".
\end{quote}
% Everyone knows that debugging is twice as hard as writing \\
% a program in the first place. So if you're as clever as you \\
% can be when you write it, how will you ever debug it? }\\
% --- Brian Kernighan, "The Elements of Programming Style".
\index{Kernighan, Brian}

\section{Hazlo Claro, Mantenlo Simple}

Escribir un programa en la vida real no es lo mismo que
aprender el arte de la programación o aprender un lenguaje nuevo.

Debido a que la meta de este libro es conducirte al aprendizaje de
conceptos más avanzados o sintaxis nueva, en la mayoría de ocasiones he tratado
de hacer las cosas de maneras diferentes. Pero esto no significa
que deberías utilizar tu conocimiento más avanzado en cada uno de tus
programas. Es todo lo opuesto.

La regla de oro es ``KISS'': Keep It Simple, Stupid (Mantenlo Simple, Estúpido).
El principio de ingeniería KISS (originado en el US NAVY alrededor de 1960)
establece que la mayoría de sistemas funcionan mejor si se mantienen
simples en lugar de complejos; por lo tanto la simplicidad
debería ser una meta en el diseño y la complejidad innecesaria
debería evitarse. No obstante, esto no significa que deberías escribir
código simplista.
\index{KISS: keep it simple, stupid}

Como un ejemplo, si estás buscando una subcadena de texto
literal dentro de una cadena, usa la función integrada {\tt index},
en lugar de utilizar el armamento completo del motor de regex para eso.
Similarmente, si sabes la posición y la longitud de la subcadena de texto,
usa la función {\tt substr}. Pero si necesitas una
coincidencia más ``parcial'' \engterm{fuzzy} con quizás algunas alternativas o
una clase de caracteres, entonces un regex podría ser la herramienta
indicada para el trabajo.
\index{función!\texttt{index}}
\index{función!\texttt{substr}}
\index{regex}

Otro principio relacionado es ``YAGNI'': you aren't gonna need
it (no lo vas a necesitar). Este acrónimo viene de la escuela
de la programación conocida como la "programación extrema"
(\engterm{extreme programming}, XP). Aunque no sigas todos los principios
de XP, esta idea es válida y justificada: no agregues una funcionalidad hasta
que realmente la necesites.
\index{YAGNI: you aren't gonna need it}

Trata de hacer tus programas tan claros como sea posible,
y tan simples como puedas. Usa conceptos más avanzados solo
si es realmente necesario, pero no lo hagas solo con el propósito
de presumir.  No trates de ser astuto ó, por lo menos,
no tan \emph{astuto}.

Ten presente que el código no solo es usado por el compilador,
sino que también por humanos. Piensa en ellos.

Contempla sobre la persona que mantendrá tu código. Como algunas personas le
gusta decir: ``Siempre escribe código como si la persona que termina
manteniendo tu código es un psicópata violento que sabe donde vives.''
Y, si esto no te convence, recuerda que la persona que mantenga tu código
dentro de un año podría ser tú. Para ese entonces, puede ser que no recuerdes
cómo ese truco astuto que usaste en aquel entonces funciona realmente.

Una cita final de Edsger Dijkstra sobre el tema:
``La simplicidad es un prerequisito para la fiabilidad.''
\index{Dijkstra, Edsger}
\index{simplicidad}

\section{Qué Hacer y Qué No Hacer}

\begin{description}

\item[No te repitas] Evita la duplicación de código.
Si tienes el mismo código en partes diferentes de tu programa,
entonces algo malo puede suceder. Quizás el código repetido
puede ir dentro de un bucle o una subrutina separada,
o a lo mejor en un módulo o una librería. Recuerda que copiar
y pegar es una fuente de maldad.
\index{DRY: don't repeat yourself}

\item[No invente la rueda de nuevo] Usa librerías y módulos
existentes cuando puedas; es posible que hayan sido probadas
y funcionarán mejor que el pequeño arreglo que escribirás.
El ecosistema de Perl~6 tiene una gran colección de módulos
de software (ver \url{modules.perl6.org})
que crece día a día. Estos módulos puedes utilizarlos
en tus programas.
\index{inventando la rueda nuevamente}

\item[Usa identificadores significativos] Si tus variables, métodos,
clases, gramáticas, módulos, y programas tienen nombres sensibles
que expresan claramente lo que son o lo que hacen,
entonces tu código será más claro y podría necesitar menos
comentarios. Nombres muy cortos como \verb|$i| o \verb|$n|
son usualmente ideales para las variables de un bucle, pero
todo lo demás necesita un nombre que explique el contenido
o el propósito. Nombres como \verb|$array| o \verb|%hash|
se han usados a lo largo de este libro para indicar
con más claridad la naturaleza de la estructura de datos,
pero no es recomendado utilizarlos en programas en la vida real.
Si tu hash contiene una colección de palabras, llámalo
\verb|%palabras| o \verb|%lista-palabras|, no \verb|%hash|.
De cualquier modo, el sigilo \% ya indica que es un hash.
\index{identificadores significativos}

\item[Escribe comentarios útiles y evita los inútiles] Un
comentario como este:
\begin{verbatim}
my $count = 1;     # asigna 1 a $count
\end{verbatim}
es completamente inútil. En general, tus comentarios no deberían
explicar lo que tu código está haciendo o cómo lo está haciendo
(esto debería ser obvio si tu código es claro), sino
\emph{la razón por la cual estás haciendo eso}: quizás deberías
hacer referencia a un teorema matemático, un ley de física, una
decisión de diseño, o una regla de negocio.
\index{comentario}

\item[Remueve código muerto y código de andamiaje]
Al escribir código nuevo, puedes crear variables que no
usas en la versión final de tu código. Si esto sucede,
remuévelas; no las dejes distraer la atención de tu lector.
Si modificas un programa existente, limpia el lugar después
de modificarlo. Recuerda la regla de los niños exploradores:
deja el lugar mejor y más limpio de como lo encontraste.
\index{código!muerto}
\index{código!andamiaje}

\item[Prueba agresivamente] Nadie puede escribir una pieza de
código significativa sin tener un número inicial de errores.
Se cita a Edsger Dijkstra diciendo que: ``Si la depuración
es el proceso de remover errores de software, entonces
la programación debe ser el proceso de crearlos.''
Lamentablemente es cierto. Aunque Dijkstra también dijo
que ``las pruebas de software muestran la presencia, no la ausencia de
errores,'' las pruebas de software son una parte esencial del
desarrollo de software. Escribe conjuntos de pruebas extensos, úsalos
a menudo, y actualízalos a medida que la funcionalidad evoluciona.
Ve la \secref{test_module} (\pagref{test_module}) para aprender sobre
algunas herramientas de pruebas automatizadas.
\index{pruebas}
\index{Dijkstra, Edsger}

\item[Evita la optimización prematura] En las palabras de
Donald Knuth: ``La optimización prematura es la fuente de todo lo
malvado (o por lo menos la mayoría de ello) en la programación.''
\index{optimización prematura}
\index{Knuth, Donald}

\item[No use números mágicos:] Considera esto:
\begin{verbatim}
my $tiempo-restante = 31536000;
\end{verbatim}

¿Cuál es el significado del número 31,536,000 que aparece de la nada?
No hay manera de saber con solo mirar esta línea de código.
Compáralo con esto:

\begin{verbatim}
my $segundosEnUnAño = 365 * 24 * 60 * 60;
# ...
my $tiempo-restante = $segundosEnUnAño;
\end{verbatim}

¿No es la segunda versión más clara?\footnote{Esta calculación se refiere al
año \emph{común} (es decir, un calendario que no es bisiesto o año civil). No
es la misma definición de otros años astronómicos, los cuales más de un cuarto
de día más largo.} Bueno, para ser sincero, sería mejor usar una constante en
tal caso:
\begin{verbatim}
constant SEGUNDOS-POR-AÑO = 365 * 24 * 60 * 60;
\end{verbatim}
\index{número!mágico}
\index{constante}

\item[Evita valores literales] Los valores literales
son malos. Si tienes que usar algunos, defínelos como
variables o constantes al comienzo de tu programa, y usa esas
variables o constantes. Las rutas literales de archivos
son especialmente malas. Si tienes que usar algunas, usa
algunas variables con rutas relativas:
\begin{verbatim}
my $dir-base = '/path/a/datos/aplicacion';
my $dir-entrada = "$dir-base/ENTRADA";
my $dir-result = "$dir-base/RESULT";
my $dir-temp = "$dir-base/TEMP";
my $dir-log = "$dir-base/LOG";
\end{verbatim}
Por lo menos, si la ruta debe cambiar, solo tienes que cambiar
la línea de código superior.
\index{valor!hard-coded}

\item[No ignore los errores devueltos por las subrutinas
o funciones integradas] No todos los valores de retorno son
útiles; por ejemplo, usualmente no chequeamos el valor de retorno
de una sentencia de impresión, pero eso es usualmente bueno porque
estamos solamente interesados en el efecto secundario tal como
el hecho de imprimir algo a la pantalla o a un archivo,
en lugar de un valor de retorno.
En la mayoría de los casos, necesitas saber si algo malo pasó
para tomar los pasos necesarios para recuperarte de la
condición de error si es posible o abortar el programa con gracia
(e.g., con un mensaje de error informativo) si el error
es muy serio para que el programa continúe.
\index{error!ignorar}

\item[Formatea tu código clara y consistentemente]
El compilador puede no importarle la indentación de código,
sin embargo eso es importante para los lectores humanos. El formato
de tu código debería ayudar a clarificar la estructura y
flujo de control de tus programas.
\index{indentación}

\item[Sé bueno y diviértete.]
\index{Perl~6!diversión}

\end{description}

\section{Usa Expresiones Idiomáticas}
\index{idioma}

Cualquier lenguaje tiene sus métodos de ``buenas prácticas''.
Estas son las expresiones idiomáticas que los programadores con
experiencia usan y se han convertido en las maneras preferidas
de hacer las cosas. Estos modismos son importantes. Ellos
te protegen de inventar la rueda nuevamente. También son los modismos que
los programadores con experiencia esperan leer; son familiares
y te permiten enfocarte en el diseño de código en lugar de
atascarte en preocupaciones detalladas de código. Usualmente,
formalizan patrones que evitan equivocaciones o errores comunes.

Aunque Perl~6 es un lenguaje relativamente nuevo, un número de
tales modismos se han perfeccionados a lo largo del tiempo.
Las siguientes son algunas de estas construcciones idiomáticas
\footnote{Cuando se sugiere dos soluciones, la segunda es usualmente la
más idiomática.}.
\index{Perl~6!idiomático}


\begin{description}
\item[Crear un hash de una lista de llaves y una lista de valores:]
\index{rebanada!hash}

Usando rebanadas (\emph{slices})
\begin{verbatim}
my %hash;
%hash{@llaves} = @valores;
\end{verbatim}

Usando el operador \emph{zip} y un metaoperador con el constructor pareja:
\index{operador!\texttt{zip}}
\index{metaoperador}

\begin{verbatim}
my %hash = @llaves Z=> @valores;
\end{verbatim}

Para las pruebas existentes, los valores de los hashes solo necesitan ser
verdaderos. Esta es una manera buena de crear un hash de una lista de
llaves:
\begin{verbatim}
my %existentes = @llaves X=> True;
\end{verbatim}

O, mejor aún, usa un conjunto:
\begin{verbatim}
my $existentes = @llaves.Set;
say "existe" if $existentes{$objeto};
\end{verbatim}

\item[Hacer los atributos mandatorios (o parámetros de la subrutina):]
Esto es una forma buena de hacer un atributo mandatorio en una clase:
\index{atributo!mandatorio}
\begin{verbatim}
has $.atr = die "El atributo 'atr' es mandatorio";
\end{verbatim}
Este código usa el mecanismo de valor por defecto: si un valor es
provisto, entonces el código para el valor por defecto no se ejecuta.
Si valor no es provisto, entonces el código muere con el mensaje
de error apropiado. El mismo mecanismo puede usarse para los
parámetros de las subrutinas.

O puedes usar el rasgo \verb|is required|:

\begin{verbatim}
> class A { has $.a is required };
> A.new;
The attribute '$!a' is required,
but you did not provide a value for it.
\end{verbatim}

Hasta puedes pasar un mensaje con una explicación:

\begin{verbatim}
> class A { has $.a is required("Lo necesitamos") };
> A.new;
The attribute '$!a' is required because Lo necesitamos,
but you did not provide a value for it.
\end{verbatim}

\item[Iterar sobre los subíndices de un array] La primera
solución que viene a la mente podría ser:
\index{método!\texttt{end}}

\begin{verbatim}
for 0 .. @array.end -> $i {...}
\end{verbatim}

Eso está bien, pero esto es probablemente lo mejor:
\index{función o método!\texttt{keys}}

\begin{verbatim}
for @array.keys -> $i {...}
\end{verbatim}

\item[Iterar sobre los subíndices y valores de un array]
El método \verb|.kv|, en combinación con un bloque puntiagudo
que toma dos parámetros, te permite fácilmente iterar sobre un
array:
\index{función o método!\texttt{kv}}

\begin{verbatim}
for @array.kv -> $i, $valor {...}
\end{verbatim}


\item[Imprimir el número de artículos en un array] Dos posibles
soluciones:
\index{función o método!\texttt{elems}}

\begin{verbatim}
say +@array;
# o:
say @array.elems;
\end{verbatim}

\item[Hacer algo cada tercera vez] Usa el operador de divisibilidad \verb|%%|
en la variable del bucle:
\index{divisibilidad}
\begin{verbatim}
if $i %% 3 {...}
\end{verbatim}

\item[Hacer algo cada \emph{n} vez:] Usa el operador de rango de derecha
exclusiva:
\index{operador!\texttt{..} (rango)}

\begin{verbatim}
for 0 ..^ $n {...}
# o, más simple:
for ^$n {...}
\end{verbatim}

\item[Divide una cadena de texto en palabras (dividiendo en espacio):]
Un invocación de método sin un invocante explícito siempre usa
la variable tópica \verb|$_| como un invocante implícito. Así que, asumiendo
que la cadena de texto ha sido asignada a \verb|$_|:
\index{función o método!\texttt{split}}
\index{función o método!\texttt{words}}
\index{variable!tópica}
\index{invocante}

\begin{verbatim}
@palabras = .split(/\s+/);
# o, más simple:
@palabras = .words;
\end{verbatim}

\item[Un bucle infinito] Una sentencia {\tt loop} sin paréntesis y sin
argumentos se ejecuta indefinidamente:
\index{bucle!infinito}

\begin{verbatim}
while True {...}
# o, más idiomático:
loop {...}
\end{verbatim}

Por supuesto, el cuerpo de la sentencia {\tt loop} debe tener algún tipo de
sentencia de flujo de control para salir del bucle en un momento determinado.

\item[Returnar los elementos únicos de una lista] El método {\tt unique}
remueve los elementos duplicados de la lista de entrada:
\index{función!\texttt{unique}}

\begin{verbatim}
return @array.unique;
\end{verbatim}

O, si sabes que la lista está ordenada, puedes usar la función {\tt squish}
(la cual remueve los elementos duplicados adyacentes).
\index{función!\texttt{squish}}

\item[Añadir los elementos de una lista] Usa la función {\tt reduce} o el
metaoperador de reducción:
\index{función!\texttt{reduce}}
\index{función o método!\texttt{sum}}

\begin{verbatim}
my $suma = @a.reduce(* + *);
# o, más simple:
my $suma = [+] @a;
# o hasta más simple, usando la suma integrada:
my $suma = @a.sum;
\end{verbatim}

\item[Intercambiar dos variables] Usa la invocación mutante del método
\verb|.=| con la función \verb|reverse|:
\begin{verbatim}
( $x, $y ) = $y, $x;
# o:
( $x, $y ) .= reverse; # equivalente a: ($x, $y) = ($x, $y).reverse
\end{verbatim}
\index{variable!intercambio}

\item[Generar enteros aleatorios entre 2 y 6] Usa el operador de rango
\verb|..| y el método {\tt pick}:
\index{función o método!\texttt{pick}}
\index{número!aleatorio}
\begin{verbatim}
$z = 2 + Int(5.rand);
# o, mejor:
$z = (2..6).pick;
\end{verbatim}

\item[Contar progresivamente de 3 en 3 en un bucle infinito] Usa
el operador de secuencia con el operador whatever star ``*''
y el operador puntiagudo:
\index{operador!\texttt{*} (whatever)}
\index{operador!\texttt{...} (secuencia)}

\begin{verbatim}
for 3, * + 3 ... * -> $n {...}
# o:
for 3, 6, 9 ... * -> $n {...}
\end{verbatim}

\item[Recorrer un rango de valores, ignorando los límites del rango:] Usa
el operador de rango exclusivo:
\index{operador!\texttt{..} (rango)}

\begin{verbatim}
for ($comienzo+1) .. ($final-1) -> $i {...}
# o, mejor aún:
for $comienzo ^..^ $final -> $i {...}
\end{verbatim}
\end{description}

\section{¿Qué sigue?}

En un libro como este no se te puede decir todo sobre la programación ni
todo sobre Perl~6. A esta altura, deberías saber cómo escribir un programa
para resolver un problema de dificultad promedia, pero mucho se ha hecho en
la última década para resolver problemas más complejos. Así que, hacia dónde
deberíamos dirigirnos desde aquí?

Lee libros sobre algoritmos. Muchos libros excelentes tratan el tema,
pero especialmente recomiendo los siguientes dos (aunque deberías saber que no
son fáciles):
\begin{itemize}
\item Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest,
and Clifford Stein, \emph{Introduction to Algorithms}, The MIT Press
\item Donald Knuth, \emph{The Art of Computer Programming}, Addison Wesley
(many volumes, many editions).
\end{itemize}

Lee otros libros sobre la programación, aún aquellos cuyos objectivos son
otros lenguajes de programación o no lenguaje en específico. Es probable que
tengan una perspectiva diferente sobre varios temas; esto te ofrecerá
un punto de vista diferente y quizás un mejor entendimiento, y complementará
lo que leíste aquí. Lee tutoriales, artículos, blogs, y foros sobre la
programación.
Participa cuando puedas. Lee la \emph{Introducción a Perl~6} la cual existe
en ocho lenguajes diferentes al momento de escribir este libro
(\url{http://perl6intro.com/}). Lee la documentación oficial de
Perl~6 (\url{https://docs.perl6.org}).
\index{Perl~6!documentación}

Este libro tiene más de mil ejemplos de código, lo cual es mucho, pero podría
ser no suficiente si quieres aprender mucho más. Deberías leer fragmentos de
código escritos por otros. Chequea las librerías o módulos de código abierto
e intenta entender lo que hacen y cómo lo hacen. Intenta usarlas.

Habiendo dicho eso, debería enfatizar que puedes leer tantos libros
como quieras sobre la teoría de la natación, pero nunca aprenderás
a nadar hasta que lo intentes. Lo mismo es cierto sobre el aprendizaje de la
programación y un lenguaje de programación. Escribe código nuevo. Modifica
los ejemplos existentes y observa lo que sucede. Intenta cosas nuevas.
Continúa, sé valiente, sumérgete en la piscina y nada. El resultado es que:
aprenderás al hacer.

¡Aprender el arte de la programación es divertido. Disfrútalo!
\index{Perl~6!diversión}

