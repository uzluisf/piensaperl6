# Piensa en Perl 6 -- https://uzluisf.gitlab.io/piensaperl6/

### NOTA

El lenguaje de programación que conocíamos como Perl 6 ha cambiado de nombre y
ahora se llama [Raku](https://raku.org/). Esto ha causado muchos cambios, lo
cual incluye este libro. Con el cambio de nombre, [Think Raku](http://greenteapress.com/wp/think-perl-6/)
recibió varias actualizaciones las cuales no son partes de esta versión. Puedes
encontrar el repositorio para la traducción actualizada en
https://gitlab.com/uzluisf/piensa-raku.

Este repositorio está ahora archivado y se recomienda que visites el repositorio
mencionado más arriba para tener la última versión del libro.

### [:blue_book: Descarga directa](https://gitlab.com/uzluisf/piensaperl6/raw/master/PDF/piensaperl6.pdf)

Archivo LATEX de la traducción en español de [Think Perl 6](http://greenteapress.com/wp/think-perl-6/) por Laurent Rosenfeld con Allen Downey.

Traducción por [Luis F. Uceta](http://uzluisf.gitlab.io/). Las siguientes
personas han ayudado a mejorar esta traducción:

- Boyd Duffee (@duffee) 

Si deseas obtener el PDF con la última revisión, sigue las instrucciones
en la sección [Cómo crear el PDF](#cómo-crear-el-pdf).

---

# Tabla de Contenido

- [Preámbulo](#preámbulo)
- [Cómo crear el PDF](#cómo-crear-el-pdf)
- [Colaboración](#colaboración)
    - [Revisión](#revisión)
    - [Cómo enviar un Pull Request or Merge Request](#cómo-enviar-un-pull-request-pr-or-merge-request-mr)


# Preámbulo

Para realizar el resaltado de sintaxis, se usa el paquete `minted`. Dicho
paquete necesita que la librería de Python `Pygments` esté instalada. Si no
está instalada en tu systema, puedes hacerlo con este comando: `pip install pygments`.

El programa `minted` usa uno de los estilos proveídos en este repo
(directorio `pygments`). Para que tal estilo sea aplicado al código, debes 
hacer lo siguiente:

1. Copiar los estilos en el directorio `pygments` al directorio de Pygments en
tu sistema. En mi caso, el directorio destinatario es
`/usr/lib/python3.6/site-packages/pygments/styles/`. Si en tu caso es el mismo,
puedes ejecutar desde `pygments` el comando:
`sudo cp *.py /usr/lib/python3.6/site-packages/pygments/styles/`.

2. Si deseas utilizar otro estilo, puedes especificarlo en
`libro/prelude.sty` dentro de `\newminted` (por ejemplo, `style=bn`).

Debido a que los diagramas en el libro son convertidos de archivos SVG a PDF,
debes tener el program [cairosvg](https://cairosvg.org/) instalado. Para
prevenir cualquier problema con el tipo, se recomienda que instale la fuente
'Inconsolata'.

# Cómo crear el PDF

El directorio `libro` contiene los archivos LATEX necesarios para compilar el
libro. Para recompilar el libro, ejecuta el siguiente comando dentro de dicho
directorio:

```
make
```
Este comando creará el directorio `tmpDir` donde se encuentra el PDF
(piensaperl6.pdf) del libro. 

> Nota: La posibilidad de una compilación exitosa incrementa si tienes una
> instalación casi completa de una distribución reciente de Tex Live.
> Para convertir de SVG (cubierta y diagramas) a PDF, se usa el programa
> [cairosvg](https://cairosvg.org/). Aségurate de que esté instalado en tu
> sistema. De lo contrario, la compilación fallará.

Para eliminar todos los archivos intermedios (incluyendo el PDF)
que resultan del proceso de compilación, ejecuta:

```
make clean
```

Si deseas mantener el PDF, puedes ejecutar `make update` (antes de ejecutar
`make clean`) lo cual copiará el PDF en la raíz del directorio `piensaperl6`.

# Colaboración

Si quieres colaborar, puedes escoger cualquier capítulo que no haya sido elegido
por otra persona. Es recomendado que, después de elegir un capítulo, te cerciores de
de agregar tu nombre en la columna **Tomado por** de la tabla más abajo y 
actualizar el archivo README . Esto es para asegurarnos de que solo una persona
está revisando/corrigiendo un capítulo en particular y así distribuir cualquier 
esfuerzo efectivamente. Una vez hayas finalizado con un capítulo, agrega `~~` 
alrededor del nombre del mismo, así que `~~Funciones~~` resulta en ~~Funciones~~.


| Capítulo                                             | Tomado por  |
| --------                                             | --------    |
| 1. La Forma del Programa                             | ~~duffee~~  |
| 2. Variables, Expressiones y Sentencias              | ~~duffee~~  |
| 3. Funciones                                         | ~~duffee~~  |
| 4. Bucles, Condicionales y Recursión                 | ~~duffee~~  |
| 5. Subrutinas Fructuosas                             | ~~duffee~~  |
| 6. Iteración                                         | ~~duffee~~  |
| 7. Cadenas de Texto                                  | ~~uzluisf~~ |
| 8. Caso Práctico: Juego de Palabras                  | ~~duffee~~  |
| 9. Arrays y Listas                                   | ~~uzluisf~~ |
| 10. Hashes                                           | ~~duffee~~  |
| 11. Caso Práctico: Selección de Estructuras de Datos | ~~duffee~~  |
| 12. Clases y Objectos                                | ~~duffee~~  |
| 13. Regexes y Gramáticas                             | ~~duffee~~  |
| 14. Programación Funcional en Perl                   | ~~uzluisf~~ |
| 15. Algunos Consejos Finales                         | ~~uzluisf~~ |
| A.  Soluciones a los Ejercicios                      | ~~duffee~~  |

## Revisión

Aunque existen muchas cosas[^1] que se pueden/deben tomar en cuenta durante el 
proceso de revisión, la presente revisión se enfocará en los siguientes puntos:

* Eliminar los errores y las imprecisiones de vocabulario.
* Aumentar la riqueza léxica y eliminar muletillas y vicios léxicos.
* Corregir los errores gramaticales y ajustar el texto a las normas y
  a los usos asentados.
* Solventar las inconsistencias sintácticas (concordancia, correlación de
  tiempos verbales, régimen preposicional, etc.); darle mayor fluidez y
  adecuación al texto mediante la elección de recursos sintácticos precisos y
  bien trabajados (conectores del discurso, oraciones subordinadas, eliminación
  de pleonasmos, etc.).
* Corregir los errores ortográficos y de puntuación.

[^1]: https://marianaeguaras.com/correccion-de-estilo-y-ortotipografica-diferencias/

## Cómo enviar un Pull Request (PR) or Merge Request (MR)

Idealmente, un MR es atómico. O sea, en lugar de subir cada *commit* directamente a
la rama *master*, tú creas nuevas ramas (por ejemplo, una rama para
"capítulo 5 revision") que son específicas a los cambios en las mismas
y después crea los *MRs* desde estas ramas. Esto es para hacer el repositorio
mucho más manejable, más fácil de revertir y últimamente simplificar los 
revisiones de códigos (ó palabras y oraciones en este caso). Recuerda que esto
es solamente una sugerencia y por lo tanto, no es algo a seguir al pie de la
letra. 
